// ==UserScript==
// @name        Count posts - reddit.com
// @namespace   Violentmonkey Scripts
// @match       https://www.reddit.com/
// @grant       none
// @version     1.0
// @author      EmeraldSnorlax
// @description 25/09/2020, 13:00:45
// ==/UserScript==


var posts = []

function findPosts() {
  posts = document.querySelectorAll("h3._eYtD2XCVieq6emjKBH3m");
}


function addNo() { 
  let index = 0
  for (let item of posts) {
    if (
      !item.innerHTML.includes('(#' + 0) &&
      !item.innerHTML.includes('(#' + 1) &&
      !item.innerHTML.includes('(#' + 2) &&
      !item.innerHTML.includes('(#' + 3) &&
      !item.innerHTML.includes('(#' + 4) &&
      !item.innerHTML.includes('(#' + 5) &&
      !item.innerHTML.includes('(#' + 6) &&
      !item.innerHTML.includes('(#' + 7) &&
      !item.innerHTML.includes('(#' + 8) &&
      !item.innerHTML.includes('(#' + 9)
    ) {
      item.innerHTML += ' <h6 style="color: red; font-size: 15px;">(#' + index + ')</h6>'
    }
    index++
  }
}

findPosts()
addNo()

setInterval(addNo, 3000)
setInterval(findPosts, 3000);
